# Home Studio 2

ElmerFEM model for the steady state field inside a realistic room, treated with increasing degrees of complexity.

Part of the [accompanying repositories](https://gitlab.com/computational-acoustics) of the [Computational Acoustics with Open Source Software](https://computational-acoustics.gitlab.io/website/) project.

## Covering Episodes

* [Home Studio - Part 1](https://computational-acoustics.gitlab.io/website/posts/12-home-studio-part-1/).

## Study Summary

The main parameters of the study are reported below.

### Source and Medium

|Parameter Name               | Symbol       | Value                   | Unit                      |
|-----------------------------|--------------|-------------------------|---------------------------|
| Source Radius               | $`a`$        | 0.005                   | meters                    |
| Source Frequencies          | $`f`$        | 125 160 200 250 315 400 | hertz                     |
| Source Surface Velocity     | $`U`$        | 10                      | meters per second         |
| Medium Sound Phase Speed    | $`c_{0}`$    | 343                     | meters per second         |
| Medium Equilibrium Density  | $`\rho_{0}`$ | 1.205                   | kilograms per cubic meter |

### Domain

| Shape            | Size (approx.)            | Mesh Algorithm  | Mesh Min. Size  | Mesh Max. Size   | Element Order | Source Location |
|------------------|---------------------------|-----------------|-----------------|------------------|---------------|-----------------|
| Realistic Room   | 4.35 X 2.79 X 2.25 meters | NETGEN 1D-2D-3D | 0.01 millimetre | 68.6 millimetres | Second        | Within the Room |

### Boundary Condition

Rigid walls.

### Note

Convergence is expected to fail at 400 Hz.

## Software Overview

The table below reports the software used for this project.

| Software                                           | Usage                        |
|----------------------------------------------------|------------------------------|
| [FreeCAD](https://www.freecadweb.org/)             | 3D Modeller                  |
| [Salome Platform](https://www.salome-platform.org) | Pre-processing               |
| [ElmerFEM](http://www.elmerfem.org)                | Multiphysical solver         |
| [ParaView](https://www.paraview.org/)              | Post-processing              |
| [Julia](https://julialang.org/)                    | Technical Computing Language |

## Repo Structure

* `elmerfem` contains the ElmreFEM project.
* `geometry.FCStd` is the FreeCAD geometry model. This file can be used to export geometric entities to _BREP_ files to pre-process with Salome. _BREP_ files are excluded from the repo as they are redundant.
* `meshing.hdf` is the Salome study of the geometry. It contains the geometry pre-processing and meshing. The mesh is exported into `elmerfem` as `elmerfem/Mesh_1.unv`. Note that the mesh in this file is **not** computed.
* The folder `frequencies` contains the study frequencies as an array of `csv` files which is loadable in ParaView.

The repo contains only the _source_ of the simulation. To obtain results, the study must be solved.

## How to Run this Study

Follow the steps below to run the study.

Clone the repository and `cd` into the cloned directory:

```bash
git clone https://gitlab.com/computational-acoustics/home-studio-2.git
cd home-studio-2/
```

`cd` in the `elmerfem` directory and run the study:

```bash
cd elmerfem/
ElmerSolver
```

When finished, open ParaView and select `File > Load State` to load the `.pvsm` file in the root of the repository. To load the ParaView state successfully, choose _Search files under specified directory_ as shown in the example below. The folder specified in `Data Directory` should be the root folder of the repo (blurred below as the absolute path will differ in your machine).

![Load State Example](res/pictures/paraview-load-state.png)

